package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleSsmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleSsmApplication.class, args);
	}
}
